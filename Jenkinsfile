pipeline {
    agent any
    tools{
        maven 'maven'
    }
    stages {
        stage('Build') {
            steps {
                // Build the Maven app
                echo "$env.BRANCH_NAME , $env.CHANGE_TITLE, $env.CHANGE_TARGET, $env.CHANGE_BRANCH"
                sh 'mvn clean package'
            }
        } 

        stage('Unit Test') {
            steps {
                // Run JUnit tests
                sh 'mvn test'
            }
        }

        stage('Integration Test'){
            steps {
                sh 'mvn verify -DskipUnitTests'
            }
        }

        
        stage('SonarQube scan') {
            steps {
                // Run SonarQube scan
                withSonarQubeEnv(installationName: 'sonar') { 
                    sh 'mvn  org.sonarsource.scanner.maven:sonar-maven-plugin:3.9.0.2155:sonar'
                }
            }
        }

        stage("Quality Gate"){
            steps{
                script{
                    timeout(time: 15, unit: 'SECONDS') {
                        def qg = waitForQualityGate() 
                        if (qg.status != 'OK') {
                            error "Pipeline aborted due to quality gate failure: ${qg.status}"
                        }
                    }
                }
            }
        }


        stage('Get app version And deploy to Nexus') {
            steps {
                script{
                    def pom = readMavenPom file: 'pom.xml'
                    def appVersion = pom.version
                    if (env.BRANCH_NAME != 'master') {
                        if(env.BRANCH_NAME.startsWith("feature/")){
                            brancheName= env.BRANCH_NAME.replace("/","-")
                        } else{
                            brancheName= env.BRANCH_NAME
                        }
                        appVersion += "-${brancheName}-${env.BUILD_NUMBER}"
                    }

                    echo "App version: ${appVersion}"
                    sh "ls -l"
                    nexusArtifactUploader(
                    nexusVersion: 'nexus3',
                    protocol: 'http',
                    nexusUrl: '18.118.158.125:8081',
                    groupId: 'com.example',
                    version:  "${appVersion}",
                    repository: 'maven-releases',
                    credentialsId: 'nexus',
                    artifacts: [
                        [artifactId: pom.artifactId,
                        classifier: '',
                        file: "${WORKSPACE}/target/frontend-1.1.0-SNAPSHOT.jar",
                        type: 'jar']
                    ]
                )
                }
            }
        }
 
    }
 
}
